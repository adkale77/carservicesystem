package com.car;

public class GlobalVariablesCar {
    public final static String HATCHBACK = "Hatchback";
    public final static String SEDAN = "Sedan";
    public final static String SUV = "SUV";
    public static String carType[] = {HATCHBACK, SEDAN, SUV};
    public static final int FUEL_PRICE = 106;
    public static final int SHAMPOO_PRICE = 54;

    // public final static String[] carType = ["Hatchback", ""]
}
