1. Create a Mechanic application that interacts with the Database as a maven project

2. When a customer enters, His information from database is retrieved via hibernate

3. The car(s) for which service needs to be done is retrieved via hibernate

4. The car goes through various servicing steps

5. The status of servicing and fuel level of the car is updated on the database

6. The entire code base for this is uploaded to bitbucket.org repository.

7. Good to have -> try to implement a feature where a customer has more than 1 car

Database on loading uses dummy values also a user can add data if they want.
Table looks like: 
Customer:
customer_id customer_name customer_phone
123	XYZ	123321
124	ABC	125671
125	UVW	126789

Car:
carNumber fuel_capacity fuel_remaining car_type customer_id
MH04 QW 3100	80	0	Hatchback	124
MH06 TH 4310	220	220	SUV	124
MH12 FG 2112	220	40	SUV	123
MH43 AB 2021	180	40	Sedan	123
MH46 CC 2020	120	20	Hatchback	123
				
		